import './App.scss'
import './styles/reset.styles.scss'

import { ThemeProvider, createTheme } from '@mui/material/styles'

import React, { useEffect } from 'react'
import { Route, Routes } from 'react-router-dom'
import { useAppDispatch } from './config/hook'

import { fetchNews } from './store/newsSlice'

import ListNews from './pages/ListNews/ListNews'
import CurrentNews from './pages/CurrentNews/CurrentNews'

const App: React.FC = () => {
    const dispatch = useAppDispatch()

    useEffect(() => {
        dispatch(fetchNews())
    }, [dispatch])

    const THEME = createTheme({
        typography: {
            fontFamily: `"Montserrat", sans-serif`,
        },
    })

    return (
        <div className="App">
            <ThemeProvider theme={THEME}>
                <Routes>
                    <Route path="/" element={<ListNews />} />
                    <Route path="/current/:id" element={<CurrentNews />} />
                </Routes>
            </ThemeProvider>
        </div>
    )
}

export default App
