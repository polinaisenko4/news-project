import React, { useEffect, useState } from 'react'
import styles from './Filter.module.scss'

import SearchIcon from '@mui/icons-material/Search'
import { useAppDispatch } from '../../config/hook'

import { setSearchWord } from '../../store/newsSlice'
import { nls } from '../../config/nls'

const Filter: React.FC = () => {
    const [search, setSearch] = useState('')
    const [result, setResult] = useState(0)
    const dispatch = useAppDispatch()

    useEffect(() => {
        if (!search) {
            setResult(0)
        } else {
            dispatch(setSearchWord(search.split(' ')))
            setResult(document.querySelectorAll('.searchedWords').length)
        }
    }, [search])

    return (
        <div className={styles.filter}>
            <h3>{nls.filter_header}</h3>
            <div>
                <SearchIcon />
                <input
                    value={search}
                    onChange={(e) => setSearch(e.target.value)}
                    placeholder={'Write key words'}
                />
            </div>
            <p>
                {nls.results}
                <span>{result}</span>
            </p>
        </div>
    )
}

export default Filter
