import React, { useEffect } from 'react'
import moment from 'moment/moment'

import { Link } from 'react-router-dom'

import {
    Card,
    CardContent,
    CardMedia,
    Typography,
    CardActions,
    Button,
} from '@mui/material'
import CalendarTodayIcon from '@mui/icons-material/CalendarToday'
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt'

import Highlighter from 'react-highlight-words'

import { useAppDispatch, useAppSelector } from '../../config/hook'
import { setNewsId } from '../../store/newsSlice'
import { nls } from '../../config/nls'

type News = {
    imageUrl: string
    title: string
    summary: string
    id: number
    publishedAt: string
}

interface CardNewsProps {
    data: News
    searchWords: string[]
}

const CardNews: React.FC<CardNewsProps> = ({ data, searchWords }) => {
    const dispatch = useAppDispatch()

    const getNewsById = (id: number) => {
        dispatch(setNewsId(id))
    }

    return (
        <div>
            <Card
                sx={{
                    maxWidth: 400,
                    height: '530px',
                    marginBottom: '25px',
                    display: 'flex',
                    justifyContent: 'space-between',
                    flexDirection: 'column',
                }}
            >
                <CardMedia
                    sx={{ height: '35%' }}
                    image={data.imageUrl}
                    title={data.title}
                />
                <CardContent>
                    <Typography
                        sx={{
                            fontSize: '14px',
                            display: 'flex',
                            gap: '8px',
                            color: '#797979',
                            alignItems: 'center',
                            margin: '24px 0',
                        }}
                        component='div'
                    >
                        <CalendarTodayIcon />
                        {moment(data.publishedAt).format('MMM Do, YYYY')}
                    </Typography>
                    <Typography gutterBottom variant='h5' component='div'>
                        <Highlighter
                            highlightClassName='searchedWords'
                            searchWords={searchWords}
                            textToHighlight={data.title}
                        />
                    </Typography>
                    <Typography variant='body2'>
                        <Highlighter
                            highlightClassName='searchedWords'
                            searchWords={searchWords}
                            textToHighlight={data.summary}
                        />
                    </Typography>
                </CardContent>
                <CardActions>
                    <Link to={`/current/${data.id}`}>
                        <Typography
                            component='div'
                            sx={{ display: 'flex', alignItems: 'center' }}
                        >
                            <Button
                                variant='text'
                                sx={{
                                    color: 'black',
                                    textTransform: 'none',
                                    fontWeight: 'bold',
                                }}
                                onClick={() => getNewsById(data.id)}
                            >
                                {nls.read_more}
                                <ArrowRightAltIcon
                                    sx={{ color: 'black', marginLeft: '8px' }}
                                />
                            </Button>
                        </Typography>
                    </Link>
                </CardActions>
            </Card>
        </div>
    )
}
export default CardNews
