import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit'

type providerType = {
    id: string
    provider: string
}

type News = {
    id: number
    featured: boolean
    title: string
    url: string
    imageUrl: string
    newsSite: string
    summary: string
    publishedAt: string
    launches: providerType[]
    events: providerType[]
}

type NewsState = {
    newsList: News[]
    currentNewsId: null | number
    currentNews: currentNews
    searchWord: string[]
    loading: boolean
    error: string | null
}

type currentNews = {
    imageUrl?: string
    title?: string
    summary?: string
}
export const fetchNews = createAsyncThunk<
    News[],
    undefined,
    { rejectValue: string }
>('news/newsTodos', async function (_, { rejectWithValue }) {
    const response = await fetch(
        'https://api.spaceflightnewsapi.net/v3/articles'
    )

    if (!response.ok) {
        return rejectWithValue('Server Error!')
    }

    const data = await response.json()

    return data
})

export const fetchNewsById = createAsyncThunk<
    currentNews,
    number | null,
    { rejectValue: string }
>('todos/fetchNewsById', async function (id, { rejectWithValue }) {
    const response = await fetch(
        `https://api.spaceflightnewsapi.net/v3/articles/${id}`
    )

    if (!response.ok) {
        return rejectWithValue('Error')
    }

    const data = await response.json()

    return data
})

const initialState: NewsState = {
    newsList: [],
    currentNewsId: null,
    currentNews: {},
    searchWord: [],
    loading: false,
    error: null,
}

const newsSlice = createSlice({
    name: 'newsList',
    initialState,
    reducers: {
        setNewsId(state, action: PayloadAction<number>) {
            state.currentNewsId = action.payload
        },
        setSearchWord(state, action: PayloadAction<string[]>) {
            state.searchWord = action.payload
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchNews.pending, (state) => {
                state.loading = true
                state.error = null
            })
            .addCase(fetchNews.fulfilled, (state, action) => {
                state.newsList = action.payload
                state.loading = false
            })
            .addCase(fetchNewsById.pending, (state) => {
                state.loading = true
                state.error = null
            })
            .addCase(fetchNewsById.fulfilled, (state, action) => {
                state.currentNews = action.payload
                state.loading = false
            })
    },
})

export const { setNewsId, setSearchWord } = newsSlice.actions
export default newsSlice.reducer
