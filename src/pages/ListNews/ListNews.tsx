import React from 'react'
import styles from './ListNews.module.scss'

import { useAppSelector } from '../../config/hook'

import { Container, Grid } from '@mui/material'

import CardNews from '../../components/CardNews/CardNews'
import Filter from '../../components/Filter/Filter'

const ListNews: React.FC = () => {
    const news = useAppSelector((state) => state.news.newsList)
    const searchWords = useAppSelector((state) => state.news.searchWord)

    return (
        <div className={styles?.listNews}>
            <Container maxWidth={'xl'}>
                <Filter />
                <Grid
                    container
                    direction='row'
                    justifyContent='space-around'
                    sx={{ rowGap: 7 }}
                >
                    {news.map((el) => {
                        return (
                            <Grid item lg={3.5} key={el.id}>
                                <CardNews searchWords={searchWords} data={el} />
                            </Grid>
                        )
                    })}
                </Grid>
            </Container>
        </div>
    )
}

export default ListNews
