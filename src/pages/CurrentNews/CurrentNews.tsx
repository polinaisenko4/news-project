import React, { useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../config/hook'
import styles from './CurrentNews.module.scss'
import { Button, Container, Typography } from '@mui/material'
import { fetchNewsById } from '../../store/newsSlice'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import { Link } from 'react-router-dom'
import { nls } from '../../config/nls'

const CurrentNews: React.FC = () => {
    const currentNews = useAppSelector((state) => state.news.currentNews)
    const id = useAppSelector((state) => state.news.currentNewsId)

    const dispatch = useAppDispatch()

    useEffect(() => {
        dispatch(fetchNewsById(id))
    }, [id, dispatch])

    return (
        <div className={styles.currentNewsButton}>
            <div className={styles.currentNews__img}>
                <img src={currentNews.imageUrl} alt={currentNews.title} />
            </div>
            <Container maxWidth={'xl'}>
                <div className={styles.currentNews__article}>
                    <h1>{currentNews.title}</h1>
                    <p>{currentNews.summary}</p>
                </div>
                <Link to={`/`}>
                    <Typography
                        component="div"
                        sx={{
                            display: 'flex',
                            alignItems: 'center',
                            margin: '35px 0 40px 35px',
                        }}
                    >
                        <Button
                            variant="text"
                            sx={{
                                color: 'black',
                                textTransform: 'none',
                                fontWeight: 'bold',
                            }}
                        >
                            <ArrowBackIcon
                                sx={{ color: 'black', marginRight: '8px' }}
                            />
                            {nls.back_to_home}
                        </Button>
                    </Typography>
                </Link>
            </Container>
        </div>
    )
}

export default CurrentNews
