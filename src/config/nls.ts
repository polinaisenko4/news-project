export const nls = {
    filter_header: 'Filter by keywords',
    results: 'Results:',
    read_more: 'Read more',
    back_to_home: 'Back to homepage',
}
